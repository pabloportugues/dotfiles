## Setting up your dotfiles with DotBot

1 - Create a repository wherever
1 - Clone it and init it in your system (my directory is ~/.dotfiles so it will be the whenever I speak about that folder)
1 - I use [DotBot](https://github.com/anishathalye/dotbot) for dotfiles management, so I'll be using that.
    1.1 - `git submodule add https://github.com/anishathalye/dotbot dotbot` - this will create a folder called `dotbot` will the submodule
    1.1 - `touch install` - to create your install script (if it doesn't create one)
    1.1 - `touch default.conf.yaml` - to create your default dotfiles mapping
    1.1 - `touch work.conf.yaml` - to create your work dotfiles mapping (if you need them)
    1.1 - `touch home.conf.yaml` - to create your personal dotfiles mapping

    The reason I have the three separate files is because I'm using the same repository for both work and personal dotfiles. I'm not using particular sensitive info there, so this just saves me the trouble of maintaing two different repos. If you follow DotBot setup tutorial it's tailored to just one, so pick what's best for you. (You can always change it later on)

    1.1 - edit the `install` file so it supports multilple profiles:
    ```bash
    #!/usr/bin/env bash
    set -e

    DEFAULT_CONFIG_PREFIX="default"
    CONFIG_SUFFIX=".conf.yaml"
    DOTBOT_DIR="dotbot"

    DOTBOT_BIN="bin/dotbot"
    BASEDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

    cd "${BASEDIR}"
    git submodule update --init --recursive "${DOTBOT_DIR}"

    for conf in ${DEFAULT_CONFIG_PREFIX} ${@}; do
        "${BASEDIR}/${DOTBOT_DIR}/${DOTBOT_BIN}" -d "${BASEDIR}" -c "${conf}${CONFIG_SUFFIX}"
    done
    ```

    1.1 - put the following in your `default.conf.yaml`
    ```yaml
    - defaults:
    link:
      create: true
      relink: true

    - link:
        ~/.iterm2: iterm2
        ~/.zprezto: zprezto
        ~/.yadr: yadr
        ~/.aliases: aliases

    - clean: ['~']

    - shell:
      - [git submodule update --init --recursive, Installing submodules]
    ```
    1.1 - for your `work.conf.yaml` you just add what it's specific for that profile
    ```yaml
    - link:
        ~/.gitconfig: gitconfig
    ```

    1.1 - apply the same logic for your `home.conf.yaml`

    `~/.iterm2: iterm2` means that I want the symlink to be created in `~/.iterm2` and that the file/folder it will link to is in the same place as my install file (i.e. the root of my repo) `iterm2`. I could have something like `~/.ssh/config: ssh/config` - this would create the link under my .ssh folder to the file that's under the ssh folder inside my repo.

1 - Since I use Zsh, specifically [Prezto](https://github.com/sorin-ionescu/prezto), let's add that here. Because I have to configure the `zshrc` file, that will make changes to the repo and I can't commit to origin since the repo is not mine. So we'll fork it before, so we can keep a link to the origin, make our custom changes and easily pull from origin to keep it updated.
    1.1 - Now that we have a fork, let's add it as a submodule to our dotfiles repo: `git submodule add https://gitlab.com/bytemybits/prezto.git zprezto`
1 - Because Prezto depends on yadr, let's do the same for it
    1.1 - `git submodule add https://github.com/skwp/dotfiles yadr`
1 - Now we'll run `git submodule update --recursive` to make sure the submodules have all dependencies

If you look at my `*.conf.yaml` files you'll notice that I mention some files/folders that I currently don't have setup. I'll explain some, but the idea is to move all your dotfiles to your repo, so that DotBot creates a symlink for them.

1 - iterm2
    - If you have iterm2 installed you'll probably have an `.iterm2` folder in your $HOME. Just `mv ~/.iterm2 ~/.dotfiles/iterm2` (beware the dotfiles location)

1 - .aliases
    - This is just to create aliases for some nifty scripts I'll explain later on. For now you can just `touch ~/.dotfiles/aliases`. This is the content of my file:
    ```bash
    # Universal Shell Aliases, should work with both Bash and ZSH.
    alias send-slack='. ~/.dotfiles/send-to-slack.sh'
    ```
    And because I'm using Zsh, go to your `zshrc` file and put the following:
    `$HOME/.aliases` [reference for bash as well](https://askubuntu.com/a/195357)

1 - .gitconfig
    - Just because I have different configs for work and home, this sets up the default entity (user and email) as well as custom alias and whatever else I may have there. (TODO: have a joint gitconfig to share all the non-specific stuff like alias and separate files for specific stuff like user and email)

Most important thing for DotBot to work as expected is to have all dotfiles you want to create symlinks for inside your dotfiles folder. This is because DotBot will only create the symlinks if the origin files are there, and won't automatically copy them to your dotfiles folder and then create the symlink (I did expect it to work this way and I was wrong).

If you have all that setup, just run `./install work` and you'll get something like:
```bash
bender@PlanetExpress ./install work
Link exists ~/.iterm2 -> /Users/bender/.dotfiles/iterm2
Link exists ~/.zprezto -> /Users/bender/.dotfiles/zprezto
Link exists ~/.yadr -> /Users/bender/.dotfiles/yadr
Creating link ~/.aliases -> /Users/bender/.dotfiles/aliases
All links have been set up
All targets have been cleaned
Installing submodules [git submodule update --init --recursive]
All commands have been executed

==> All tasks executed successfully
Creating link ~/.gitconfig -> /Users/bender/.dotfiles/gitconfig
All links have been set up

==> All tasks executed successfully
```

Because I didn't have `.aliases` and `.gitconfig` in my $HOME folder, it created the symlinks, the rest it just stayed the same because I had already created them before.

## Setting up custom scripts

I now faced myself with a different problem. Because I don't have the company's slack on my home account, how can I share interesting stuff I find online with everyone there? (we have channels were we talk about other than work). Then I thought, well, this is slack, so I can probably use a webhook right? Indeed!

Go to `https://<your-team-here>.slack.com/apps/manage/custom-integrations` and create a new `Incoming WebHook`. As a precaution I recommend setting the channel as yourself (i.e., your user handle) just so if you screw up you don't spam other channels.

Now that we have our link, let's create a script that simplifies the message sending.

Do `touch ~/.dotfiles/send-to-slack.sh`

It looks something like this:
```bash
#!/bin/bash
channel=$2
url="https://hooks.slack.com/services/<some-hash>/<another-hash>/<i-like-hashes>"
payload=""

if [ -z $channel ]
then
  payload='payload={"text": "'$1'", "unfurl_links": true}'
else
  payload='payload={"channel": "'$channel'", "text": "'$1'", "unfurl_links": true}'
fi

curl -X POST --data-urlencode $payload $url
```

It's a little crud but it does the job, which is sending a message to a specific channel. It works like this:
`send-slack "Let's go already!!" random`

Now, as you may have noticed, there are some hashes on that slack webhook url, that don't exactly like to share with the world, just so I don't, accidentally, get overflowed with messages.

## Encrypting the sensitive data

For reference: [https://www.outcoldman.com/en/archive/2015/09/17/keep-sensitive-data-encrypted-in-dotfiles/](https://www.outcoldman.com/en/archive/2015/09/17/keep-sensitive-data-encrypted-in-dotfiles/)

























